package main

// import "fmt"

func main() {

}

func expand(input string) string {
	var result []string
	// split on word boundaries
	words := split(input, ' ')

	if len(words) == 0 {
		return ""
	}

	// for each word do recursion
	if len(words) > 1 {
		for i, word := range words {
			_ = i
			result = append(result, expand(word))
		}
		return concat(" ", result...)
	}

	// only one word left
	word := words[0]

	// split on braces
	tokens := split(word, byte('{'), byte('}'))
	// fmt.Println("tokens", tokens)

	// empty word
	if len(tokens) == 0 {
		return ""
	}

	// if len == 1 => no expansion (base case)
	if len(tokens) == 1 {
		return unescapeBraces(word)
	}

	var expansions []string
	if len(tokens) >= 2 {
		sequence := tokens[1]
		// fmt.Println("sequence", sequence)
		for _, s := range split(sequence, ',') {
			prefix := tokens[0]
			suffix := ""
			if len(tokens) > 2 {
				suffix = word[len(prefix)+1+len(sequence)+1:]
			}
			expansions = append(expansions,
				unescapeBraces( // remove braces with backslashes
					expand(prefix+s+suffix), // recursively expand until we reach the base case
				),
			)
		}
	}

	return concat(" ", expansions...)
}

// func translate(str string, orig string, new string) string {
// 	for i, _ := range str {
// 		if len(str) <= i + len(orig) {
// 			break
// 		}
// 		for j, _ := range len(orig) {
// 			if str[i] ==
// 		}
// 	}
// }

func unescapeBraces(str string) string {
	var cur, last byte
	for i := range str {
		cur = str[i]
		if last == '\\' && cur == '{' || last == '\\' && cur == '}' {
			str = str[:i-1] + str[i:]
			return unescapeBraces(str)
		}
		last = cur
	}
	return str
}

func concat(sep string, str ...string) string {
	var res string
	for i, val := range str {
		if i == len(str)-1 {
			res += val
		} else {
			res += val + sep
		}
	}
	return res
}

func split(str string, sep ...byte) []string {
	if len(str) == 0 {
		return []string{}
	}

	var res []string
	var prev byte
	var last int
	for i, val := range str {
		for _, s := range sep {
			if val == rune(s) && prev != '\\' {
				res = append(res, str[last:i])
				last = i + 1
				break
			}
		}
		prev = str[i]
	}
	res = append(res, str[last:])
	return res
}
