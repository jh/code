package main

import "testing"

// https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html
var expansionTests = []struct {
	input string
	want  string
}{
	{`hello world`, `hello world`},
	{`a{d,c,b}e`, `ade ace abe`},
	// {`a{1,{2,3}}e`, `a1e a2e a3e`},
	{`a{1,2}{3,4}e`, `a13e a14e a23e a24e`},
	{`readme.{md,txt}  .git`, `readme.md readme.txt  .git`},
	{`foobar\{1,2\}`, `foobar{1,2}`},
	{`foo {bar,baz} `, `foo bar baz `},
}

// TODO: make it work with UTF-8 input: https://go.dev/blog/strings

func TestExpand(t *testing.T) {
	for _, test := range expansionTests {
		res := expand(test.input)
		if res != test.want {
			t.Errorf("expansion(%v) returned %v, expected %v", test.input, res, test.want)
		}
	}
}

var splitTests = []struct {
	input string
	sep   byte
	want  []string
}{
	{`hello world`, ' ', []string{`hello`, `world`}},
	{`a,b,,c,d`, ',', []string{`a`, `b`, ``, `c`, `d`}},
	{`,x,y,z,`, ',', []string{``, `x`, `y`, `z`, ``}},
}

func TestSplit(t *testing.T) {
	for _, test := range splitTests {
		res := split(test.input, test.sep)
		for i := range res {
			if res[i] != test.want[i] {
				t.Errorf("split(%v, %v) returned %v, expected %v", test.input, test.sep, res, test.want)
			}
		}
	}
}

var concatTests = []struct {
	input []string
	sep   string
	want  string
}{
	{[]string{`hello`, `world`}, " ", `hello world`},
}

func TestConcat(t *testing.T) {
	for _, test := range concatTests {
		res := concat(test.sep, test.input...)
		if res != test.want {
			t.Errorf("concat(%v, %v) returned %v, expected %v", test.sep, test.input, res, test.want)
		}
	}
}
