package main

// import "fmt"

func main() {
	// dummy to make Go compiler happy
}

// Given a SORTED slice of integers,
// determine whether the slice contains the target element t.
// If it contains t, return the position of the element, otherwise -1.
func binarySearchRecursive(arr []int, t int, l int, h int) int {
	if len(arr) == 0 || l > len(arr) || h >= len(arr) || l > h { // sanity checks
		return -1
	}
	// fmt.Println("l", l, "h", h)
	if h == l {
		if arr[l] == t {
			return l // found
		} else {
			return -1 // not found
		}
	}

	m := (h-l)/2 + l
	// fmt.Println("m", m)
	if arr[m] >= t {
		return binarySearchRecursive(arr, t, l, m) // search in the lower half
	} else {
		return binarySearchRecursive(arr, t, m+1, h) // search in the upper half
	}
}

func binarySearchLoop(arr []int, t int) int {
	var l, h, m int
	if len(arr) == 0 { // sanity check
		return -1
	}
	l = 0
	h = len(arr)
	for l != h {
		m = (h-l)/2 + l
		// fmt.Println("l", l, "h", h, "m", m)
		if arr[m] >= t {
			h = m // search lower half
		} else {
			l = m + 1 // search upper half
		}
	}
	// now: l == h
	if l < len(arr) && arr[l] == t {
		return l // found it
	}
	return -1 // not found
}
