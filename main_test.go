package main

import "testing"

var binarySearchTests = []struct {
	arr    []int
	search int
	want   int
}{
	{[]int{-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 4}, // standard
	{[]int{}, 42, -1},                        // empty array
	{[]int{2, 3, 4}, 2, 0},                   // search for first element
	{[]int{5, 6, 7, 8, 9}, 7, 2},             // search for middle element
	{[]int{12, 13, 14}, 14, 2},               // search for last element
	{[]int{1, 2, 3, 4}, 5, -1},               // not found
	{[]int{1, 8, 32, 33}, 8, 1},              // not continuous
	{[]int{1, 1, 1, 2, 3, 3, 4, 8, 9}, 3, 4}, // duplicates
}

func TestBinarySearchRecursive(t *testing.T) {
	for _, test := range binarySearchTests {
		res := binarySearchRecursive(test.arr, test.search, 0, len(test.arr)-1)
		if res != test.want {
			t.Errorf("binarySearchRecursive(%v, %d) returned %d, expected %d", test.arr, test.search, res, test.want)
		}
	}
}

func TestBinarySearchLoop(t *testing.T) {
	for _, test := range binarySearchTests {
		res := binarySearchLoop(test.arr, test.search)
		if res != test.want {
			t.Errorf("binarySearchLoop(%v, %d) returned %d, expected %d", test.arr, test.search, res, test.want)
		}
	}
}
